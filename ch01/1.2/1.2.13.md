# Answer
A simple greedy solution works like this

1. Scan the first row which has not been scanned.
2. Start from the rightmost square on this row, find the first uncontrolled square and place a rook at this slot
3. Goto step 1 if there are still available row(s), otherwise terminate.

The basic idea behind this solution is the observation that for a single row placing a rook at a square on right controls no
less number of squares than placing it at a square on left. Since each row must have a square, this algorithm cannot be beaten.
